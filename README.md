# SDL Tridion Core service samples
This repo contains a collection off useful Tridion Core service scripts, most of them are not optomised for production use, but are instead intended as examples of how core service can be used for one of tasks, and running reports.

Please feel free to take and use these however you feel fit, I am accepting Commits so feel free to commit your own ones.

## Linqpad setup instructions ##

I'll assume you are using Linqpad, but of course you could use this same code in C# Console app etc.

* Download Linqpad
* Open the "My Extensions" file and copy in the code from /Setup/My Extensions.cs this contains some bootstrap code that is used in all the examples, and puts it all in one place in case you need to make setup changes.
* Add in the following references by pressing f4 ("Tridion.ContentManager.CoreService.Client.dll" "Tridion.ContentManager.CoreService.Workflow.dll") found in your Tridion installation
* click the additional namespace imports and add the following
*   - System.Net
*   - System.ServiceModel
*   - Tridion.ContentManager.CoreService
*   - Tridion.ContentManager.CoreService.Client
* add in the missing references when it asks you if you want to
* click the execute arrow

You are now all set.