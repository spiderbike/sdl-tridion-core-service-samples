<Query Kind="Program">
  <Namespace>System.Net</Namespace>
  <Namespace>System.ServiceModel</Namespace>
  <Namespace>Tridion.ContentManager.CoreService</Namespace>
  <Namespace>Tridion.ContentManager.CoreService.Client</Namespace>
</Query>

private static ICoreService client = MyExtensions.GetClient(Util.GetPassword("TridionCMSURL"),  Util.GetPassword("TridionUser"), Util.GetPassword("TridionPassword"));



void Main()
{

	
	CreateOrEnableUser("myDomain","fred@example.com","fred@example.com",client);
	CreateOrEnableUser("myDomain","bob@example.com","bob@example.com",client);

	

	}
	
	
	public static void CreateOrEnableUser(string domain, string name,string description, ICoreService client)
	{
		UserData userData = GetUserByName(domain,name,client);
		if (userData ==null)
		{
			CreateUser(domain,name,description,client);
		}
		else
		{
			EnableUser(userData,client);
		}
		GrantUserRights(userData,client);
	}
	
	
	public static void GrantUserRights(UserData userData, ICoreService client)
	{
		bool isAdministrator = userData.Privileges == 1 ? true : false;
		bool isEnabled = userData.IsEnabled ?? false;
		if (!isAdministrator && isEnabled)
		{
	//			StringBuilder userInfo = new StringBuilder();
	//			userInfo.Append(userData.Id + ",");
	//			userInfo.Append(userData.Title + ",");
	//			userInfo.Append(isAdministrator + ",");
	//			
	//			Console.WriteLine(userInfo.ToString());
			var memberships = new List<GroupMembershipData>();
			
			
			memberships.Add(GetGroupMembershipDataFromGroupLink("tcm:0-48-65568"));
			memberships.Add(GetGroupMembershipDataFromGroupLink("tcm:0-1325-65568"));
			memberships.Add(GetGroupMembershipDataFromGroupLink("tcm:0-1528-65568"));
			memberships.Add(GetGroupMembershipDataFromGroupLink("tcm:0-1449-65568"));
			
			userData.GroupMemberships = memberships.ToArray();
			Console.WriteLine("Granting access rights for user : {0} ({1})", userData.Title,userData.Description);
			client.Update(userData, new ReadOptions());
		}
	}	
	
	
	
static GroupMembershipData GetGroupMembershipDataFromGroupLink(string tcm)
{
	var groupMembershipData = new GroupMembershipData();
	var groupLink = new LinkToGroupData();
	groupLink.IdRef = tcm;
	groupMembershipData.Group = groupLink;
	return groupMembershipData;
}
	
	
	public static void CreateUser(string domain, string name,string description, ICoreService client)
	{
	Console.WriteLine("Create user :{0}", domain + "\\" + name);
	var user = (UserData)client.GetDefaultData(ItemType.User, null, new ReadOptions());
	user.Title = domain + "\\" + name;
	user.Description = description;
	user.Privileges = 0;
	client.Create(user, new ReadOptions());
	}
	
	public static void EnableUser(UserData userData, ICoreService client)
	{
		if (userData.IsEnabled == true)
		{
			Console.WriteLine("User already enabled :{0}", userData.Title);
		}
			else
		{
			Console.WriteLine("Enable user :{0}", userData.Title);
			userData.IsEnabled = true;
			client.Update(userData, new ReadOptions());
		}
	}
	
	public static UserData GetUserByName(string domain, string name, ICoreService client)
	{
	    var users = client.GetSystemWideList(new UsersFilterData { BaseColumns = ListBaseColumns.IdAndTitle, IsPredefined = false });
	    var user = users.FirstOrDefault(item => item.Title == domain + "\\" + name);
	    return user as UserData;
	}