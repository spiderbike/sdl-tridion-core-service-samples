<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Net.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.ServiceModel.dll</Reference>
  <Reference>DLLs\Tridion.ContentManager.CoreService.Client.dll</Reference>
  <Namespace>System.Net</Namespace>
  <Namespace>System.ServiceModel</Namespace>
  <Namespace>Tridion.ContentManager</Namespace>
  <Namespace>Tridion.ContentManager.CoreService</Namespace>
  <Namespace>Tridion.ContentManager.CoreService.Client</Namespace>
  <Namespace>System.Globalization</Namespace>
</Query>

private static ICoreService Client = MyExtensions.GetClient("192.168.52.2", "coreServiceUsername", Util.GetPassword("password"));


void Main()
{

	var schema = (SchemaData)Client.Read("tcm:460-222626-8", null);
	var schemaFields = Client.ReadSchemaFields(schema.Id, true, null);
	
	schemaFields.Dump();
	
	schema.Xsd.Dump("before");

	var schemaDataFields = new List<ItemFieldDefinitionData>(schemaFields.Fields);
	

	var embSchemaFieldName = new EmbeddedSchemaFieldDefinitionData();
	embSchemaFieldName.Name = "Name2";
	embSchemaFieldName.Description = "Desc";
	embSchemaFieldName.MaxOccurs = 1;
	embSchemaFieldName.EmbeddedSchema = new LinkToSchemaData { IdRef = "tcm:460-222685-8" };
	schemaDataFields.Add(embSchemaFieldName);

	
	schemaFields.Fields = schemaDataFields.ToArray();


	schema.Xsd = Client.ConvertSchemaFieldsToXsd(schemaFields).ToString();

	

   


	schema.Xsd.Dump("after");
	//	schema.VersionInfo.Dump();
	//	schema.Metadata.Dump();
	
	Client.Update(schema, null);
}
