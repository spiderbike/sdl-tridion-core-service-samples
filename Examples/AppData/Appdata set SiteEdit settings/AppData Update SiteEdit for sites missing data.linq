<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Net.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.ServiceModel.dll</Reference>
  <Reference>C:\Users\markn\OneDrive\Work\Tridion\Tridion DLLS\2013 SP1 HR1\CoreService\Tridion.ContentManager.CoreService.Client.dll</Reference>
  <Namespace>System.Net</Namespace>
  <Namespace>System.ServiceModel</Namespace>
  <Namespace>Tridion.ContentManager.CoreService</Namespace>
  <Namespace>Tridion.ContentManager.CoreService.Client</Namespace>
</Query>

readonly string filename = @"PublicationTmMappings.xml";
readonly XNamespace xlinkNs = "http://www.w3.org/1999/xlink";
readonly XNamespace siteEditNs = "http://www.sdltridion.com/2011/SiteEdit";
readonly XNamespace markNs = "http://tridionted.blogspot.co.uk";
private static ICoreService Client = MyExtensions.GetClient("SERVERURL", "username", Util.GetPassword("password"));

class PublicationMetadata
{
    public string PageId { get; set; }
	public string ComponentId { get; set; }    
	public string PageTitle { get; set; }
	public string ComponentTitle { get; set; }
}

void Main()
{

    PublicationsFilterData filter = new PublicationsFilterData();
	IdentifiableObjectData[] publications = Client.GetSystemWideList(filter);
	foreach (PublicationData pub in publications.Where(p => (p.Title.StartsWith("700") || p.Title.StartsWith("800")) && !p.Title.Contains("LRT2") && !p.Title.Contains("Test ")))
	{
		Console.WriteLine(pub.Title);
		//Check if there is any App data for pub
		ApplicationData appData = Client.ReadApplicationData(pub.Id, "SiteEdit");
		if (appData == null)
		{	
			Console.WriteLine("   No Appdata found");
			var titleDetails = GetGenericPubTitle(pub.Title);
			GetAppDataFromLocalXml(titleDetails.GenericPubTitle);
			try
			{
				string newAppdata = GetAppDataForEmptyPublication(pub.Title);
				newAppdata.Dump("Maybe it should be ?");
				SaveAppData(newAppdata,pub.Id);
			}
			catch
			{
				pub.Title.Dump("Sorry I could not give you a recomendation for this site");
			}
		}
		else
		{
			CheckAppData(pub.Title, Encoding.UTF8.GetString(appData.Data));
		}
	}
}



public string GetAppDataFromLocalXml(string PubTitle)
{
	var doc = XDocument.Load(filename);
	var result = doc.Root.Elements(markNs + "publication").Where(p => p.Attribute(markNs + "Title").Value == PubTitle).Single().Descendants(siteEditNs + "configuration").Single();
	return result.ToString();
}

public bool CheckAppData(string pubTitle, string appData)
{
	Console.WriteLine("   Checking existing appdata is correct");
	try
	{
		var referenceAppData = GetAppDataForEmptyPublication(pubTitle);
		//Console.WriteLine(referenceAppData);
		bool appDataCorrect = true;
	

		var CorrectData = ParseMetadata(referenceAppData);

		var ActualData = ParseMetadata(appData);
		
		if (ActualData.PageId != CorrectData.PageId)
		{
			Console.WriteLine(string.Format("Error PageContext is {0} ({1}), but it should be {2} ({3})", Client.Read(ActualData.PageId, null).Title,ActualData.PageId, CorrectData.PageId,Client.Read(CorrectData.PageId, null).Title));
			appDataCorrect = false;
		}
			
		if (ActualData.ComponentId != CorrectData.ComponentId)
		{
			Console.WriteLine(string.Format("Error ComponentContext is {0} ({1}), but it should be {2} ({3})", Client.Read(ActualData.ComponentId, null).Title,ActualData.ComponentId,Client.Read(CorrectData.ComponentId, null).Title, CorrectData.ComponentId));
			appDataCorrect = false;
		}
		
		return appDataCorrect;
	}
	catch(Exception ex)
	{
		Console.WriteLine(ex);
		return false;
	}
}

string GetAppDataForEmptyPublication(string pubTitle)
{
		var titleDetails = GetGenericPubTitle(pubTitle);
		
		string appData = GetAppDataFromLocalXml(titleDetails.GenericPubTitle);
		PublicationMetadata pubMeta = ParseMetadata(appData);
		string compPublication = pubMeta.ComponentTitle.Replace("{{market}}", titleDetails.Market);
		string pagePublication = pubMeta.PageTitle.Replace("{{market}}", titleDetails.Market);
		string compPublicationId = Client.Read("/webdav/"+ compPublication, null).Id;
		string pagePublicationId = Client.Read("/webdav/"+ pagePublication, null).Id;
		return CreateMetadata(pagePublicationId, compPublicationId);
}

PublicationMetadata ParseMetadata(string appData)
{
	var doc = XDocument.Parse(appData);
	
	return doc.Root.Elements(siteEditNs + "Publication")
		.Select(p => new PublicationMetadata {
			PageId = p.Elements(siteEditNs + "PageContextPublicationId").Attributes(xlinkNs + "href").Select(a => a.Value).SingleOrDefault(),
			PageTitle = p.Elements(siteEditNs + "PageContextPublicationId").Attributes(markNs + "title").Select(a => a.Value).SingleOrDefault(),
			ComponentId = p.Elements(siteEditNs + "ComponentContextPublicationId").Attributes(xlinkNs + "href").Select(a => a.Value).SingleOrDefault(),
			ComponentTitle = p.Elements(siteEditNs + "ComponentContextPublicationId").Attributes(markNs + "title").Select(a => a.Value).SingleOrDefault()
		}).SingleOrDefault();
}

public class PubTitleDetails
{
	public string Market { get; set; }
	public string GenericPubTitle { get; set; }
}

PubTitleDetails GetGenericPubTitle(string pubTitle)
{
    var match = Regex.Match(pubTitle, @"(Site A|Site B)( Site C)?", RegexOptions.IgnoreCase);
	if (!match.Success)
		throw new Exception(String.Format("Standard publication market not found in publication {0}", pubTitle));
	
	var details = new PubTitleDetails();
	details.Market = match.Value;
	details.GenericPubTitle = pubTitle.Substring(0, match.Index) + "{{market}}" + pubTitle.Substring(match.Index + match.Length);
	return details;
}



string CreateMetadata(string pageId, string componentId)
{
	var doc = new XDocument(
		new XElement(siteEditNs + "configuration",
			new XElement(siteEditNs + "Publication", new XAttribute("xmlns", siteEditNs),
				new XElement(siteEditNs + "PageContextPublicationId", new XAttribute(xlinkNs + "href", pageId), new XAttribute(XNamespace.Xmlns + "xlink", xlinkNs)),
				new XElement(siteEditNs + "ComponentContextPublicationId", new XAttribute(xlinkNs + "href", componentId), new XAttribute(XNamespace.Xmlns + "xlink", xlinkNs))
			)
		)
	);
	return doc.ToString();
}

void SaveAppData(string data, string itemId)
{
	Byte[] byteData = Encoding.UTF8.GetBytes(data);
	ApplicationData appData = new ApplicationData
	{
	    ApplicationId = "SiteEdit",
	    Data = byteData,
	    TypeId = "XmlElement:configuration, http://www.sdltridion.com/2011/SiteEdit"
	};
	Client.SaveApplicationData(itemId, new[] { appData });
}

byte[] ConvertXDocument(XDocument doc)
{
	XmlWriterSettings xws = new XmlWriterSettings { OmitXmlDeclaration = true, Encoding = new UTF8Encoding(encoderShouldEmitUTF8Identifier: false) };
	using (var ms = new MemoryStream())
	{
		using (var xw = XmlWriter.Create(ms, xws))
		{
			doc.Save(xw);
		}
		return ms.ToArray();
	}
}