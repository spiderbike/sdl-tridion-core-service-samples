<Query Kind="Program">
  <Namespace>Tridion.ContentManager.CoreService.Client</Namespace>
</Query>

private static ICoreService Client = MyExtensions.GetClient(Util.GetPassword("TridionCMSURL"),  Util.GetPassword("TridionUser"), Util.GetPassword("TridionPassword"));


void Main()
{

	var appData = Client.ReadAllApplicationData("tcm:234-186937");
	appData.Dump();
}