<Query Kind="Program">
  <Namespace>Tridion.ContentManager.CoreService.Client</Namespace>
</Query>

private static ICoreService Client = MyExtensions.GetClient(Util.GetPassword("TridionCMSURL"),  Util.GetPassword("TridionUser"), Util.GetPassword("TridionPassword"));


void Main()
{
	
	
	//Get every schema from the schema pub
	var Schemafilter = new RepositoryItemsFilterData();
	Schemafilter.ItemTypes = new[] { ItemType.Schema, };
	Schemafilter.Recursive = true;
	Schemafilter.BaseColumns = ListBaseColumns.IdAndTitle;
	Schemafilter.SchemaPurposes = new [] { SchemaPurpose.Component};
	
	var allSchemas =new List<SchemaData>();
	var schemaArr = Client.GetList("tcm:0-72-1", Schemafilter);
	Console.WriteLine(string.Format("Location,Title,ID,Lock Type,Approval state, Modified"));

	foreach (SchemaData data in schemaArr)
	{
		Console.WriteLine("Schema : " + data.Title + "("+ data.Id +") -"+ ListAllComponents(data.Id) +" Components");
	}
	
	
	
	
	
}

private static int ListAllComponents(string SchemaId)
{
	var filter = new UsingItemsFilterData
	{
		ItemTypes = new[] { ItemType.Component },
		IncludedVersions = VersionCondition.OnlyLatestVersions,
		BaseColumns = ListBaseColumns.Id,
	};
	
	var items = Client.GetList(SchemaId, filter);
	return items.Count();
}