void Main()
{
    // Write code to test your extensions here. Press F5 to compile and run.
}

public static class MyExtensions
{
    // Write custom extension methods here. They will be available to all queries.

    public static readonly ReadOptions DefaultReadOptions = new ReadOptions { LoadFlags = LoadFlags.None };
    public static readonly ReadOptions WebDavReadOptions = new ReadOptions { LoadFlags = LoadFlags.WebDavUrls };
    public static readonly ReadOptions ExpandedReadOptions = new ReadOptions { LoadFlags = LoadFlags.Expanded };
    public static readonly ReadOptions AllthethingsReadOptions = new ReadOptions { LoadFlags = LoadFlags.Expanded | LoadFlags.IncludeAllowedActions | LoadFlags.KeywordXlinks | LoadFlags.WebDavUrls };


    public static ICoreService GetClient(string hostname, string username, string password)
    {
        var binding = new BasicHttpBinding()
        {
            MaxBufferSize = int.MaxValue,
            MaxBufferPoolSize = int.MaxValue,
            MaxReceivedMessageSize = int.MaxValue,
            ReaderQuotas = new System.Xml.XmlDictionaryReaderQuotas()
            {
                MaxStringContentLength = int.MaxValue,
                MaxArrayLength = int.MaxValue,
            },
            Security = new BasicHttpSecurity()
            {
                Mode = BasicHttpSecurityMode.TransportCredentialOnly,
                Transport = new HttpTransportSecurity()
                {
                    ClientCredentialType = HttpClientCredentialType.Windows,
                }
            }
        };
        hostname = string.Format("{0}{1}{2}", hostname.StartsWith("http") ? "" : "http://", hostname, hostname.EndsWith("/") ? "" : "/");
        var endpoint = new EndpointAddress(hostname + "/webservices/CoreService2013.svc/basicHttp");
        ChannelFactory<ICoreService> factory = new ChannelFactory<ICoreService>(binding, endpoint);
        factory.Credentials.Windows.ClientCredential = new System.Net.NetworkCredential(username, password);
        return factory.CreateChannel();
    }


    public class CoreServiceStreamUploadSession : IDisposable
    {
        private readonly StreamUploadClient _client;

        public CoreServiceStreamUploadSession(string hostname, string username, string password)
        {
            var netTcpBinding = new NetTcpBinding
            {
                MaxReceivedMessageSize = 2147483647,
                TransferMode = TransferMode.StreamedRequest,
                ReaderQuotas = new XmlDictionaryReaderQuotas
                {
                    MaxStringContentLength = 2147483647,
                    MaxArrayLength = 2147483647
                },
            };
            var remoteAddress = new EndpointAddress(string.Format("net.tcp://{0}:2660/CoreService/2013/streamUpload_netTcp", hostname));
            _client = new StreamUploadClient(netTcpBinding, remoteAddress);
            _client.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(username, password);

        }

        public void Dispose()
        {
            if (_client.State == CommunicationState.Faulted)
            {
                _client.Abort();
            }
            else
            {
                _client.Close();
            }
        }

        public string UploadBinaryContent(string fileName, MemoryStream uploadContent)
        {
            return _client.UploadBinaryContent(fileName, uploadContent);
        }

    }

    public static ISessionAwareCoreService GetImpersonatedUser(string hostname, string username, string password, string impersonationUser)
    {
        var netTcpBinding = new NetTcpBinding
        {
            MaxReceivedMessageSize = 2147483647,
            ReaderQuotas = new XmlDictionaryReaderQuotas
            {
                MaxStringContentLength = 2147483647,
                MaxArrayLength = 2147483647
            },
        };
        var remoteAddress = new EndpointAddress(string.Format("net.tcp://{0}:2660/CoreService/2013/netTcp", hostname));
        SessionAwareCoreServiceClient _client = new SessionAwareCoreServiceClient(netTcpBinding, remoteAddress);
        _client.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(username, password);
        _client.Impersonate(impersonationUser);

        return _client;
    }

  

    public class Fields : IEnumerable<Field>
    {
        private ItemFieldDefinitionData[] definitions;
        private XmlNamespaceManager namespaceManager;

        private XmlElement root; // the root element under which these fields live

        // at any point EITHER data OR parent has a value
        private SchemaFieldsData data; // the schema fields data as retrieved from the core service
        private Fields parent; // the parent fields (so we're an embedded schema), where we can find the data
        public Fields(SchemaFieldsData _data, ItemFieldDefinitionData[] _definitions, string _content = null, string _rootElementName = null)
        {
            data = _data;
            definitions = _definitions;
            var content = new XmlDocument();
            if (!string.IsNullOrEmpty(_content))
            {
                content.LoadXml(_content);
            }
            else
            {
                content.AppendChild(content.CreateElement(string.IsNullOrEmpty(_rootElementName) ? _data.RootElementName : _rootElementName, _data.NamespaceUri));
            }
            root = content.DocumentElement;
            namespaceManager = new XmlNamespaceManager(content.NameTable);
            namespaceManager.AddNamespace("custom", _data.NamespaceUri);
        }
        public Fields(Fields _parent, ItemFieldDefinitionData[] _definitions, XmlElement _root)
        {
            definitions = _definitions;
            parent = _parent;
            root = _root;
        }
        public static Fields ForContentOf(SchemaFieldsData _data)
        {
            return new Fields(_data, _data.Fields);
        }
        public static Fields ForContentOf(SchemaFieldsData _data, ComponentData _component)
        {
            return new Fields(_data, _data.Fields, _component.Content);
        }
        public static Fields ForMetadataOf(SchemaFieldsData _data, RepositoryLocalObjectData _item)
        {
            return new Fields(_data, _data.MetadataFields, _item.Metadata, "Metadata");
        }
        public string NamespaceUri
        {
            get { return data != null ? data.NamespaceUri : parent.NamespaceUri; }
        }
        public XmlNamespaceManager NamespaceManager
        {
            get { return parent != null ? parent.NamespaceManager : namespaceManager; }
        }
        internal IEnumerable<XmlElement> GetFieldElements(ItemFieldDefinitionData definition)
        {
            return root.SelectNodes("custom:" + definition.Name, NamespaceManager).OfType<XmlElement>();
        }
        internal XmlElement AddFieldElement(ItemFieldDefinitionData definition)
        {
            var newElement = root.OwnerDocument.CreateElement(definition.Name, NamespaceUri);
            XmlNodeList nodes = root.SelectNodes("custom:" + definition.Name, NamespaceManager);
            XmlElement referenceElement = null;
            if (nodes.Count > 0)
            {
                referenceElement = (XmlElement)nodes[nodes.Count - 1];
            }
            else
            {
                // this is the first value for this field, find its position in the XML based on the field order in the schema
                bool foundUs = false;
                for (int i = definitions.Length - 1; i >= 0; i--)
                {
                    if (!foundUs)
                    {
                        if (definitions[i].Name == definition.Name)
                        {
                            foundUs = true;
                        }
                    }
                    else
                    {
                        var values = GetFieldElements(definitions[i]);
                        if (values.Count() > 0)
                        {
                            referenceElement = values.Last();
                            break; // from for loop
                        }
                    }
                } // for every definition in reverse order
            } // no existing values found
            root.InsertAfter(newElement, referenceElement); // if referenceElement is null, will insert as first child
            return newElement;
        }
        public IEnumerator<Field> GetEnumerator()
        {
            return (IEnumerator<Field>)new FieldEnumerator(this, definitions);
        }
        public bool Exists(string _name)
        {
            return definitions.Any(def => def.Name == _name);
        }
        public Field this[string _name]
        {
            get
            {
                var definition = definitions.First<ItemFieldDefinitionData>(ifdd => ifdd.Name == _name);
                if (definition == null) throw new ArgumentOutOfRangeException("Unknown field '" + _name + "'");
                return new Field(this, definition);
            }
        }
        public override string ToString()
        {
            return root.OuterXml;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }
        IEnumerator<Field> IEnumerable<Field>.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    public class FieldEnumerator : IEnumerator<Field>
    {
        private Fields fields;
        private ItemFieldDefinitionData[] definitions;
        // Enumerators are positioned before the first element until the first MoveNext() call
        int position = -1;
        public FieldEnumerator(Fields _fields, ItemFieldDefinitionData[] _definitions)
        {
            fields = _fields;
            definitions = _definitions;
        }
        public bool MoveNext()
        {
            position++;
            return (position < definitions.Length);
        }
        public void Reset()
        {
            position = -1;
        }
        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }
        public Field Current
        {
            get
            {
                try
                {
                    return new Field(fields, definitions[position]);
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
        public void Dispose()
        {
        }
    }
    public class Field
    {
        private Fields fields;
        private ItemFieldDefinitionData definition;
        public Field(Fields _fields, ItemFieldDefinitionData _definition)
        {
            fields = _fields;
            definition = _definition;
        }
        public string Name
        {
            get { return definition.Name; }
        }
        public Type Type
        {
            get { return definition.GetType(); }
        }
        public string Value
        {
            get
            {
                return Values.Count > 0 ? Values[0] : null;
            }
            set
            {
                if (Values.Count == 0) fields.AddFieldElement(definition);
                Values[0] = value;
            }
        }
        public ValueCollection Values
        {
            get
            {
                return new ValueCollection(fields, definition);
            }
        }
        public void AddValue(string value = null)
        {
            XmlElement newElement = fields.AddFieldElement(definition);
            if (value != null) newElement.InnerText = value;
        }
        public void RemoveValue(string value)
        {
            var elements = fields.GetFieldElements(definition);
            foreach (var element in elements)
            {
                if (element.InnerText == value)
                {
                    element.ParentNode.RemoveChild(element);
                }
            }
        }
        public void RemoveValue(int i)
        {
            var elements = fields.GetFieldElements(definition).ToArray();
            elements[i].ParentNode.RemoveChild(elements[i]);
        }
        public IEnumerable<Fields> SubFields
        {
            get
            {
                var embeddedFieldDefinition = definition as EmbeddedSchemaFieldDefinitionData;
                if (embeddedFieldDefinition != null)
                {
                    var elements = fields.GetFieldElements(definition);
                    foreach (var element in elements)
                    {
                        yield return new Fields(fields, embeddedFieldDefinition.EmbeddedFields, (XmlElement)element);
                    }
                }
            }
        }
        public Fields GetSubFields(int i = 0)
        {
            var embeddedFieldDefinition = definition as EmbeddedSchemaFieldDefinitionData;
            if (embeddedFieldDefinition != null)
            {
                var elements = fields.GetFieldElements(definition);
                if (i == 0 && !elements.Any())
                {
                    // you can always set the first value of any field without calling AddValue, so same applies to embedded fields
                    AddValue();
                    elements = fields.GetFieldElements(definition);
                }
                return new Fields(fields, embeddedFieldDefinition.EmbeddedFields, elements.ToArray()[i]);
            }
            else
            {
                throw new InvalidOperationException("You can only GetSubField on an EmbeddedSchemaField");
            }
        }
        // The subfield with the given name of this field
        public Field this[string name]
        {
            get { return GetSubFields()[name]; }
        }
        // The subfields of the given value of this field
        public Fields this[int i]
        {
            get { return GetSubFields(i); }
        }
        public LinkToCategoryData Category
        {
            get
            {
                var keywordFieldDefinition = this.definition as KeywordFieldDefinitionData;
                if ((keywordFieldDefinition) != null)
                {
                    return keywordFieldDefinition.Category;
                }
                return null;
            }
        }
        public LinkToSchemaData[] AllowedTargetSchemas
        {
            get
            {
                var componentLinkFieldDefinition = this.definition as ComponentLinkFieldDefinitionData;
                if (componentLinkFieldDefinition != null)
                {
                    return componentLinkFieldDefinition.AllowedTargetSchemas;
                }
                var multimediaLinkFieldDefinition = this.definition as MultimediaLinkFieldDefinitionData;
                if (multimediaLinkFieldDefinition != null)
                {
                    return multimediaLinkFieldDefinition.AllowedTargetSchemas;
                }
                return null;
            }
        }
    }
    public class ValueCollection
    {
        private Fields fields;
        private ItemFieldDefinitionData definition;
        public ValueCollection(Fields _fields, ItemFieldDefinitionData _definition)
        {
            fields = _fields;
            definition = _definition;
        }
        public int Count
        {
            get { return fields.GetFieldElements(definition).Count(); }
        }
        public bool IsLinkField
        {
            get { return definition is ComponentLinkFieldDefinitionData || definition is ExternalLinkFieldDefinitionData || definition is MultimediaLinkFieldDefinitionData; }
        }
        public bool IsRichTextField
        {
            get { return definition is XhtmlFieldDefinitionData; }
        }
        public string this[int i]
        {
            get
            {
                XmlElement[] elements = fields.GetFieldElements(definition).ToArray();
                if (i >= elements.Length) throw new IndexOutOfRangeException();
                if (IsLinkField)
                {
                    return elements[i].Attributes["xlink:href"].Value;
                }
                else
                {
                    return elements[i].InnerXml.ToString(); // used to be InnerText
                }
            }
            set
            {
                XmlElement[] elements = fields.GetFieldElements(definition).ToArray<XmlElement>();
                if (i >= elements.Length) throw new IndexOutOfRangeException();
                if (IsLinkField)
                {
                    elements[i].SetAttribute("href", "http://www.w3.org/1999/xlink", value);
                    elements[i].SetAttribute("type", "http://www.w3.org/1999/xlink", "simple");
                    // TODO: should we clear the title for MMCLink and CLink fields? They will automatically be updated when we save the xlink:href.
                }
                else
                {
                    if (IsRichTextField)
                    {
                        elements[i].InnerXml = value;
                    }
                    else
                    {
                        elements[i].InnerText = value;
                    }
                }
            }
        }
        public IEnumerator<string> GetEnumerator()
        {
            return fields.GetFieldElements(definition).Select<XmlElement, string>(elm => IsLinkField ? elm.Attributes["xlink:href"].Value : elm.InnerXml.ToString()
            ).GetEnumerator();
        }
    }


}





public class TridionTcm
{
    private const string TridionRegPatern = @"(?<scheme>tcm:)?(?<publicationid>[0-9]+)-(?<itemid>[0-9]+)(?:-(?<typeid>[0-9]+))?(?:-v(?<version>[0-9]+))?";
    public int Id { get; private set; }
    public int PublicationId { get; private set; }
    public int TypeId { get; private set; }
    public String Tcm { get; private set; }
    public int? Version { get; private set; }



    public TridionTcm(string tcm)
    {
        tcm = tcm.ToLower();
        var rgx = new Regex(TridionRegPatern);
        var match = rgx.Match(tcm);
        TypeId = String.IsNullOrEmpty(match.Groups["typeid"].Value) ? 16 : int.Parse(match.Groups["typeid"].Value);
        if (TypeId != 1)
        {
            PublicationId = int.Parse(match.Groups["publicationid"].Value);
            Id = int.Parse(match.Groups["itemid"].Value);
        }
        else
        {
            //Handle Publication TCMs as they are in different format
            PublicationId = int.Parse(match.Groups["itemid"].Value);
            Id = int.Parse(match.Groups["publicationid"].Value);
        }
        Version = String.IsNullOrEmpty(match.Groups["version"].Value) ? (int?)null : int.Parse(match.Groups["version"].Value);
        Tcm = string.Format(TridionTcmHelpers.TcmFormat(TypeId), PublicationId, Id, TypeId);

        if (!IsValidTcm(Tcm))
        {
            throw new Exception("I just created an invalid TCM, sorry.");
        }
    }

    public string GetPublicationID()
    {
        string tcm = string.Format("tcm:0-{0}-1", PublicationId);
        return tcm;
    }


    /// <summary>
    ///  Pass in the TCM of a component and a component from another component and it will return back the first component tcm, in the context of the second tcm's publication
    /// </summary>
    public static string GetLocalTcmFromPublication(string tcm, string tcmOfItemAtPublcation)
    {
        var sourceTcm = new TridionTcm(tcm);
        var targetTcm = new TridionTcm(tcmOfItemAtPublcation);
        return string.Format(TridionTcmHelpers.TcmFormat(sourceTcm.TypeId), targetTcm.PublicationId, sourceTcm.Id, sourceTcm.TypeId);
    }




    /// <summary>
    ///  Pass in the TCM of a component and check it is in a valid format
    /// </summary>
    public static Boolean IsValidTcm(string tcm)
    {
        tcm = tcm.ToLower();
        var rgx = new Regex(TridionRegPatern);
        return rgx.IsMatch(tcm);
    }


    public static class TridionTcmHelpers
    {
        /// <summary>
        ///  0: PublicationId  1: ItemId   2: TypeId
        /// </summary>
        public static string TcmFormat(int typeId)
        {
            //change format for Publication TCMs
            return typeId == 1 ? "tcm:0-{0}-{2}" : "tcm:{0}-{1}-{2}";
        }

    }

}
// You can also define non-static classes, enums, etc.